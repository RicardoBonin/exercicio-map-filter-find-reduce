import movies from "../movies.json";

const foo = () => true;

/*** find: sempre retorna 1 item da array. Nunca mais do que 1 ***/

/*** filter: sempre retorna 1 ou mais itens da array. ***/

/*** map: funcao usada para transformar dados ***/

/*** reduce: funcao usada para "reduzir" dados. Pode retornar 1 ou mais elementos. Com reduce, podemos implementar: filter, find e map ***/

/***************************************************************/
/***************************************************************/
/***************************************************************/

/*** EXEMPLO ***/

// CarName :: String
// Car :: {name :: String, color :: String, year :: Int}
// Cars :: [Car]

const cars = [
  // Note que "name" e "color" sao do tipo String e "year" do tipo Int.
  { name: "Fusca", color: "White", year: "1979" },
  { name: "Golf", color: "black", year: "2010" },
  { name: "Audi TT", color: "blue", year: "2000" },
  { name: "Opala", color: "black", year: "1980" },
  { name: "Ferrari Enzo", color: "red", year: "2019" },
];

/*
 * findCar :: CarName -> Cars -> Car
 * Funcao para encontrar um carro.
 * A funcao recebe o nome de um carro e uma lista de carros, e retorna o carro encontrado.
 */

// Bom, esta funcao recebe CarName (nome de algum carro) e uma lista de carros.
const findCar = (nomeCarro, listaDeCarros) =>
  listaDeCarros.find((carro) => carro.name === nomeCarro);

/*
  Entao, quando a funcao eh invocada deste jeito (como eh feito nos testes)...

     findCar('Ferrari Ezno', cars)

   ...o resultado sera apenas um object contendo:

   { name: "Ferrari Enzo", color: "red", year: "2019" }
*/

/*** FIM DO EXEMPLO ***/

/***************************************************************/
/***************************************************************/
/***************************************************************/

/*** Types ***/
// Movie :: {
//   title :: String,
//   year :: String,
//   genres :: [String],
//   ratings :: [Int],
//   poster :: String,
//   contentRating :: String,
//   duration :: String,
//   releaseDate :: String,
//   averageRating :: Int,
//   originalTitle :: String,
//   storyline :: String,
//   actors :: [String],
//   imdbRating :: Float,
//   posterurl :: String
// }

// MovieName :: String

// Movies :: [Movie]

// ReleaseDate :: String

// MovieRatings :: {
//   title :: String,
//   minRating :: Int,
//   maxRating :: Int
// }

// Genre :: String

// Actor :: String

// ActorMovies :: {
//   actor :: String,
//   movies :: [
//     {
//       title :: String
//     }
//   ]
// }

/*** Exercises ***/
/*
 * findMovie :: MovieName -> Movies -> Movie
 * Funcao para encontrar um filme.
 * A funcao recebe o nome de um filme e uma lista de filmes, e retorna o filme encontrado.
 */
const movieName = "Nyckeln till frihet";
const findMovie = (movieName, movie) =>
  movie.find((movie) => movie.title === movieName);

/*
 * findMovieReleasedAt :: ReleaseDate -> Movies -> Movie
 * Funcao para encontrar um filme lancado numa data especifica.
 * A funcao recebe uma data (no formato YYYY-MM-DD) e uma lista de filmes, e retorna o filme lancado nesta data.
 */
const date = "1995-03-03";
const findMoviesReleasedAt = (date, movies) =>
  movies.filter((movie) => movie.releaseDate === date).find((movie) => movie);

/*
 * findMovieReleaseYear :: MovieName -> Int
 * Funcao para retornar o ano em que o filme foi lancado.
 * A funcao recebe o nome de um filme, e retorna somente o ano em que o filme foi lancado.
 */
const findMovieReleaseYear = (movieName, movies) =>
  movies
    .filter((movie) => movie.title === movieName)
    .map((movie) => movie.year)
    .reduce((acumulador, movie, indice, year) => year);

/*
 * hasMovie :: MovieName -> -> Movies -> Bool
 * Funcao para saber se um filme existe na lista de filmes.
 * A funcao recebe o nome de um filme e uma lista de filmes, e retorna true e o filme existir ou false do contrario.
 */
const hasMovie = (movieName, movies) => {
  const movie = movies.filter((movie) => movie.title === movieName);
  if (movie.length > 0) {
    return true;
  } else {
    return false;
  }
};

/*
 * addOne :: [Int] -> [Int]
 * Funcao para incrementar 1 a cada item da array.
 * A funcao recebe uma lista de inteiros, e retorna uma lista de inteiros com cada elemento incrementado por 1.
 * Exemplo: [1,2,3], deve retornar [2,3,4]
 */
const addOne = (movies) => movies.map((movie) => movie + 1);

/*
 * capitalizeMoviesTitle :: Movies -> Movies
 * Funcao para transformar o titulo de cada filme em maiusculo.
 * A funcao recebe uma lista de filmes, e retorna  uma lista de filmes com os titulos(title) em maiusculo.
 */
const capitalizeMoviesTitle = (movies) =>
  movies.map((movie) => ({ ...movie, title: movie.title.toUpperCase() }));

/*
 * transformToMinMaxRatings :: Movie -> MovieRatings
 * Funcao para converter um objeto Movie em object MovieRatings.
 * A funcao recebe um objeto Movie, e retorna um objeto MovieRatings.
 */
const transformToMinMaxRatings = (movie) => ({
  title: movie.title,
  maxRating: Math.max.apply(null, movie.ratings),
  minRating: Math.min.apply(null, movie.ratings),
});

/*
 * transformMoviesToMinMaxRatings :: [Movie] -> [MovieRatings]
 * Funcao para converter uma lista de Movie em uma lista de MovieRatings.
 * A funcao recebe uma Movies, e retorna uma [MovieRatings].
 */
const transformMoviesToMinMaxRatings = (movies) =>
  movies.map((movie) => ({
    title: movie.title,
    maxRating: Math.max.apply(null, movie.ratings),
    minRating: Math.min.apply(null, movie.ratings),
  }));

/*
 * getAllMoviesByGenre :: Genre -> Movies -> Movies
 * Funcao para encontrar filmes do mesmo genero.
 * A funcao recebe um genero e uma lista de filmes, e retorna uma lista de filmes com o genero solicitado.
 */
const getAllMoviesByGenre = (genre, movies) =>
  movies.filter((movie) =>
    movie.genres.toString().toLowerCase().split(",").includes(genre)
  );

/*
 * getActorMovies :: Actor -> Movies -> ActorMovies
 * Funcao para retornar filmes do mesmo ator.
 * A funcao recebe o nome de um atore uma lista de filmes, e retorna ActorMovies.
 */
const getActorMovies = (actor, movies) => {
  const listMovies = movies.filter((movie) =>
    movie.actors.toString().toLowerCase().split(",").includes(actor)
  );

  const mov = listMovies.map((list) =>  ({title: list.title}));
  const actorMovies = {
    actor: actor,
    movies: mov,
  }
  
  return actorMovies
};
/*
 * updateMovieAverageRating :: Movie -> Movie
 * Funcao para atualizar o averageRating de um filme baseado na media do campo ratings.
 * A funcao recebe um filme, e retorna o mesmo filme com o campo averageRating atualizado.
 * Exemplo: {ratings: [1,2,3], averageRating: 0}, deve retornar {ratings: [1,2,3], averageRating: 2}
 */
const updateMovieAverageRating = (movie) => {
  const average = movie.ratings.reduce((acumulador, valorAtual) => acumulador+valorAtual)/movie.ratings.length
  return {...movie, averageRating: average}
}

/*
 * updateAllMoviesAverageRating :: Movies -> Movies
 * Funcao para atualizar o averageRating de todos os filmes baseado na media do campo ratings.
 * A funcao recebe uma lista filmes, e retorna os mesmos filmes com o campo averageRating atualizado.
 */
const updateAllMoviesAverageRating = (movies) => //console.log(movies.map(movie=> ({...movie, averageRating: movie.ratings.reduce((acumulador, valorAtual) => (acumulador+valorAtual))/movie.ratings.length})))
  movies.map(movie=> ({...movie, averageRating: movie.ratings.reduce((acumulador, valorAtual) => (acumulador+valorAtual))/movie.ratings.length}))

export {
  foo,
  findCar,
  findMovie,
  findMoviesReleasedAt,
  findMovieReleaseYear,
  hasMovie,
  addOne,
  capitalizeMoviesTitle,
  transformToMinMaxRatings,
  transformMoviesToMinMaxRatings,
  getAllMoviesByGenre,
  getActorMovies,
  updateMovieAverageRating,
  updateAllMoviesAverageRating,
};
