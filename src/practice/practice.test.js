import {
  foo,
  findCar,
  findMovie,
  findMoviesReleasedAt,
  findMovieReleaseYear,
  hasMovie,
  addOne,
  capitalizeMoviesTitle,
  transformToMinMaxRatings,
  transformMoviesToMinMaxRatings,
  getAllMoviesByGenre,
  getActorMovies,
  updateMovieAverageRating,
  updateAllMoviesAverageRating,
} from "./practice";
import movies from "../movies.json";

describe("Testing test", () => {
  test("foo should be truthy", () => {
    const actual = foo();
    expect(actual).toBeTruthy();
  });
});

describe("Exmple", () => {
  test("Deve retornar o carro pesquisado", () => {
    const cars = [
      { name: "Fusca", color: "White", year: "1979" },
      { name: "Golf", color: "black", year: "2010" },
      { name: "Audi TT", color: "blue", year: "2000" },
      { name: "Opala", color: "black", year: "1980" },
      { name: "Ferrari Enzo", color: "red", year: "2019" },
    ];
    const actual = findCar("Opala", cars);
    const expected = {
      name: "Opala",
      color: "black",
      year: "1980",
    };
    expect(actual).toEqual(expected);
  });
});

describe("Exercises", () => {
  test("Should return the queried movie", () => {
    const actual = findMovie("Pulp Fiction", movies);
    const expected = {
      title: "Pulp Fiction",
      year: "1994",
      genres: ["Crime", "Drama"],
      ratings: [
        2,
        9,
        7,
        7,
        7,
        2,
        2,
        4,
        1,
        9,
        6,
        3,
        4,
        10,
        8,
        6,
        7,
        5,
        9,
        4,
        3,
        5,
        9,
        3,
        8,
        2,
        6,
        7,
        4,
        4,
      ],
      poster:
        "MV5BMTkxMTA5OTAzMl5BMl5BanBnXkFtZTgwNjA5MDc3NjE@._V1_SY500_CR0,0,336,500_AL_.jpg",
      contentRating: "15",
      duration: "PT154M",
      releaseDate: "1994-11-25",
      averageRating: 0,
      originalTitle: "",
      storyline:
        "Jules Winnfield (Samuel L. Jackson) and Vincent Vega (John Travolta) are two hit men who are out to retrieve a suitcase stolen from their employer, mob boss Marsellus Wallace (Ving Rhames). Wallace has also asked Vincent to take his wife Mia (Uma Thurman) out a few days later when Wallace himself will be out of town. Butch Coolidge (Bruce Willis) is an aging boxer who is paid by Wallace to lose his weight. The lives of these seemingly unrelated people are woven together comprising of a series of funny, bizarre and uncalled-for incidents.                Written by\nSoumitra",
      actors: ["John Travolta", "Uma Thurman", "Samuel L. Jackson"],
      imdbRating: 8.9,
      posterurl:
        "https://images-na.ssl-images-amazon.com/images/M/MV5BMTkxMTA5OTAzMl5BMl5BanBnXkFtZTgwNjA5MDc3NjE@._V1_SY500_CR0,0,336,500_AL_.jpg",
    };

    expect(actual).toEqual(expected);
  });

  test("Should return a movie with specific releaseDate ", () => {
    const actual = findMoviesReleasedAt("2002-12-18", movies);
    const expected = 2;

    expect(actual.releaseDate).toEqual("2002-12-18");
  });

  test("Should return the year a movie was released", () => {
    const movieTitle = "Pulp Fiction";
    const actual = findMovieReleaseYear(movieTitle, movies);
    const expected = "1994";
    expect(actual).toEqual(expected);
  });

  test("Should return true if movie exists", () => {
    const movieTitle = "Pulp Fiction";
    const actual = hasMovie(movieTitle, movies);
    const expected = true;
    expect(actual).toEqual(expected);
  });

  test("Should add 1 to each element of the array", () => {
    const data = [1, 2, 3, 4, 5];
    const actual = addOne(data);
    const expected = [2, 3, 4, 5, 6];
    expect(actual).toEqual(expected);
  });

  test("Should capitalize movies title", () => {
    const data = [{ title: "foo" }, { title: "bar" }];
    const actual = capitalizeMoviesTitle(data);
    const expected = [{ title: "FOO" }, { title: "BAR" }];
    expect(actual).toEqual(expected);
  });

  test("Should return a movie with title, maxRating and minRating", () => {
    const data = {
      title: "foo",
      ratings: [1, 2, 3, 4, 5],
    };

    const actual = transformToMinMaxRatings(data);
    const expected = {
      title: "foo",
      minRating: 1,
      maxRating: 5,
    };
    expect(actual).toStrictEqual(expected);
  });

  test("Should return movies with title, maxRating and minRating", () => {
    const data = [
      {
        title: "foo",
        ratings: [1, 2, 3, 4, 5],
      },
      {
        title: "bar",
        ratings: [0, 3, 5, 14, 2],
      },
    ];

    const actual = transformMoviesToMinMaxRatings(data);
    const expected = [
      {
        title: "foo",
        minRating: 1,
        maxRating: 5,
      },
      {
        title: "bar",
        minRating: 0,
        maxRating: 14,
      },
    ];
    expect(actual).toStrictEqual(expected);
  });

  test("Should return movies which has given genre", () => {
    const data = [
      {
        title: "foo",
        genres: ["drama", "Crime"],
      },
      {
        title: "bar",
        genres: ["Comedy", "Drama"],
      },
      {
        title: "baz",
        genres: ["Documentary"],
      },
    ];

    const actual = getAllMoviesByGenre("drama", data);
    expect(actual.length).toEqual(2);
  });

  test("Should return an object like {actor, movies}", () => {
    const data = [
      {
        title: "foo",
        actors: ["Actor1", "actor2"],
      },
      {
        title: "bar",
        actors: ["actor3", "actor4"],
      },
      {
        title: "baz",
        actors: ["Actor4", "actor1"],
      },
    ];

    const actual = getActorMovies("actor4", data);
    const expected = {
      actor: "actor4",
      movies: [
        {
          title: "bar",
        },
        {
          title: "baz",
        },
      ],
    };
    expect(actual).toStrictEqual(expected);
  });

  test("Should update movie averageRating", () => {
    const data = {
      title: "foo",
      ratings: [1, 2, 3, 4, 5],
      averageRating: 0,
    };
    const actual = updateMovieAverageRating(data);
    const expected = {
      title: "foo",
      ratings: [1, 2, 3, 4, 5],
      averageRating: 3,
    };
    expect(actual).toStrictEqual(expected);
  });

  test("Should update all movies averageRating", () => {
    const data = [
      {
        title: "foo",
        ratings: [1, 2, 3, 4, 5],
        averageRating: 0,
      },
      {
        title: "bar",
        ratings: [0, 3, 5, 14, 2],
        averageRating: 0,
      },
    ];
    const actual = updateAllMoviesAverageRating(data);
    const expected = [
      {
        title: "foo",
        ratings: [1, 2, 3, 4, 5],
        averageRating: 3,
      },
      {
        title: "bar",
        ratings: [0, 3, 5, 14, 2],
        averageRating: 4.8,
      },
    ];
    expect(actual).toStrictEqual(expected);
  });
});
